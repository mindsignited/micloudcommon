package com.mindsignited.keycloak.decodelocal;

import com.mindsignited.keycloak.conditions.TokenServiceConditions;
import com.mindsignited.keycloak.converters.KeycloakAccessTokenConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.security.oauth2.resource.JwtAccessTokenConverterConfigurer;
import org.springframework.context.annotation.Conditional;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.stereotype.Component;

/**
 * This class is only used if we want to decode JWT locally using public key. We are currently not using this functionality.
 * It is enabled by providing this property:
 * <pre>
 * spring:
 *    oauth2:
 *        resource:
 *            jwt:
 *                keyValue: |
 *                -----BEGIN PUBLIC KEY-----
 *                MIGfMA0G...
 *                -----END PUBLIC KEY-----
 * </pre>
 * This class isn't active otherwise.
 * <br><br>
 * *NOTE* : Currently this implementation doesn't support redirections on InvalidTokenException's(e.g. expired tokens).
 * <br><br>
 * Created by nicholaspadilla on 1/21/15.
 */
@Conditional({TokenServiceConditions.JwtToken.class})
@Component
public class KeycloakJwtCustomization implements JwtAccessTokenConverterConfigurer {

    @Value("${spring.oauth2.resource.id}")
    private String currentResourceId;

    @Override
    public void configure(JwtAccessTokenConverter converter) {
        converter.setAccessTokenConverter(new KeycloakAccessTokenConverter(currentResourceId));
    }

}
