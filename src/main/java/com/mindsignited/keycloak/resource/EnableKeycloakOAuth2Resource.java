package com.mindsignited.keycloak.resource;

/**
 * Convenient annotation for enabling security (by default globally) using Keycloak OAuth2 access
 * tokens. The security constraints on the resources and the token decoding mechanisms can
 * be configured externally via {@link org.springframework.cloud.security.oauth2.resource.ResourceServerProperties "spring.oauth2.resource.*}
 * configuration properties.
 *
 * @author Dave Syer
 * @author nicholaspadilla on 1/29/15.
 *
 */

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(KeycloakOAuth2ResourceConfiguration.class)
public @interface EnableKeycloakOAuth2Resource {
}
