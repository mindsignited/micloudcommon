package com.mindsignited.keycloak.resource;

import com.mindsignited.keycloak.conditions.TokenServiceConditions;
import com.mindsignited.keycloak.tokenservices.KeycloakResourceServerTokenServicesConfiguration;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.boot.actuate.autoconfigure.ManagementServerProperties;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.cloud.security.oauth2.resource.ResourceServerProperties;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfiguration;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.util.ClassUtils;

/**
 * Created by nicholaspadilla on 1/29/15.
 */
@Configuration
@ConditionalOnExpression("'${spring.oauth2.client.clientId:}'==''")
@Conditional(TokenServiceConditions.ResourceServerCondition.class)
@ConditionalOnClass({EnableResourceServer.class, SecurityProperties.class})
@ConditionalOnWebApplication
@EnableResourceServer
@Import(KeycloakResourceServerTokenServicesConfiguration.class)
public class KeycloakOAuth2ResourceConfiguration {

    @Autowired
    private ResourceServerProperties resource;

    @Bean
    @ConditionalOnMissingBean(ResourceServerConfigurer.class)
    public ResourceServerConfigurer resourceServer() {
        return new ResourceSecurityConfigurer(resource);
    }

    protected static class ResourceSecurityConfigurer extends ResourceServerConfigurerAdapter {

        private ResourceServerProperties resource;

        @Autowired
        public ResourceSecurityConfigurer(ResourceServerProperties resource) {
            this.resource = resource;
        }

        @Override
        public void configure(ResourceServerSecurityConfigurer resources)
                throws Exception {
            resources.resourceId(resource.getResourceId());
        }

        @Override
        public void configure(HttpSecurity http) throws Exception {
            http.authorizeRequests().anyRequest().authenticated();
        }

    }

    @Configuration
    protected static class ResourceServerOrderProcessor implements BeanPostProcessor {

        @Override
        public Object postProcessAfterInitialization(Object bean, String beanName)
                throws BeansException {
            if (bean instanceof ResourceServerConfiguration) {
                ResourceServerConfiguration configuration = (ResourceServerConfiguration) bean;
                configuration.setOrder(getOrder());
            }
            return bean;
        }

        @Override
        public Object postProcessBeforeInitialization(Object bean, String beanName)
                throws BeansException {
            return bean;
        }

        private int getOrder() {
            if (ClassUtils
                    .isPresent(
                            "org.springframework.boot.actuate.autoconfigure.ManagementServerProperties",
                            null)) {
                return ManagementServerProperties.ACCESS_OVERRIDE_ORDER - 10;
            }
            return SecurityProperties.ACCESS_OVERRIDE_ORDER - 10;
        }

    }


}
