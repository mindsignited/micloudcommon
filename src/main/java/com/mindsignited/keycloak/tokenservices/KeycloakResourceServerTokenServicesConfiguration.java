package com.mindsignited.keycloak.tokenservices;

import com.mindsignited.keycloak.conditions.TokenServiceConditions;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingClass;
import org.springframework.cloud.security.oauth2.client.ClientConfiguration;
import org.springframework.cloud.security.oauth2.resource.JwtAccessTokenConverterConfigurer;
import org.springframework.cloud.security.oauth2.resource.ResourceServerProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.annotation.AnnotationAwareOrderComparator;
import org.springframework.security.oauth2.client.OAuth2RestOperations;
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeResourceDetails;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.util.StringUtils;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created by nicholaspadilla on 1/29/15.
 */
@Configuration
@Import(ClientConfiguration.class)
public class KeycloakResourceServerTokenServicesConfiguration {

    @Configuration
    protected static class ResourceServerPropertiesConfiguration {

        @Autowired
        private AuthorizationCodeResourceDetails client;

        @Bean
        public ResourceServerProperties resourceServerProperties() {
            return new ResourceServerProperties(client.getClientId(),
                    client.getClientSecret());
        }
    }

    @Configuration
    @Conditional(TokenServiceConditions.NotJwtToken.class)
    protected static class RemoteTokenServicesConfiguration {

        @Configuration
        @Conditional(TokenServiceConditions.TokenInfo.class)
        protected static class TokenInfoServicesConfiguration {

            @Autowired
            private ResourceServerProperties resource;

            @Autowired
            private AuthorizationCodeResourceDetails client;

            @Value("${spring.oauth2.resource.id}")
            private String currentResourceId;

            @Bean
            public ResourceServerTokenServices remoteTokenServices() {
                KeycloakRemoteTokenServices services = new KeycloakRemoteTokenServices(currentResourceId);
                services.setCheckTokenEndpointUrl(resource.getTokenInfoUri());
                services.setClientId(client.getClientId());
                services.setClientSecret(client.getClientSecret());
                return services;
            }

        }

        @Configuration
        @ConditionalOnMissingClass(name = "org.springframework.social.connect.support.OAuth2ConnectionFactory")
        @Conditional(TokenServiceConditions.NotTokenInfo.class)
        protected static class UserInfoTokenServicesConfiguration {

            @Autowired
            private ResourceServerProperties sso;

            @Autowired
            private AuthorizationCodeResourceDetails client;

            @Autowired(required = false)
            private Map<String, OAuth2RestOperations> resources = Collections.emptyMap();

            @Bean
            @ConditionalOnMissingBean(ResourceServerTokenServices.class)
            public ResourceServerTokenServices userInfoTokenServices() {
                KeycloakUserInfoTokenServices services = new KeycloakUserInfoTokenServices(
                        sso.getUserInfoUri(), client.getClientId());
                services.setResources(resources);
                return services;
            }

        }

    }

    @Configuration
    @Conditional(TokenServiceConditions.JwtToken.class)
    @Slf4j
    protected static class JwtTokenServicesConfiguration {

        @Autowired
        private ResourceServerProperties resource;

        @Autowired(required = false)
        private List<JwtAccessTokenConverterConfigurer> configurers = Collections.emptyList();

        @Bean
        @ConditionalOnMissingBean(ResourceServerTokenServices.class)
        public ResourceServerTokenServices jwtTokenServices() {
            KeycloakTokenServices services = new KeycloakTokenServices();
            services.setTokenStore(jwtTokenStore());
            return services;
        }

        @Bean
        public TokenStore jwtTokenStore() {
            return new JwtTokenStore(jwtTokenEnhancer());
        }

        @Bean
        public JwtAccessTokenConverter jwtTokenEnhancer() {
            JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
            String keyValue = resource.getJwt().getKeyValue();
            if (!StringUtils.hasText(keyValue)) {
                try {
                    keyValue = (String) new RestTemplate().getForObject(
                            resource.getJwt().getKeyUri(), Map.class).get("value");
                } catch (ResourceAccessException e) {
                    // ignore
                    log.warn("Failed to fetch token key (you may need to refresh when the auth server is back)");
                }
            } else {
                if (StringUtils.hasText(keyValue) && !keyValue.startsWith("-----BEGIN")) {
                    converter.setSigningKey(keyValue);
                }
            }
            if (keyValue != null) {
                converter.setVerifierKey(keyValue);
            }
            AnnotationAwareOrderComparator.sort(configurers);
            for (JwtAccessTokenConverterConfigurer configurer : configurers) {
                configurer.configure(converter);
            }
            return converter;
        }

    }
}
