package com.mindsignited.keycloak.sso;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.context.annotation.Import;

/**
 * Enable Keycloak Single Sign On (SSO) with an OAuth2 provider declared in external properties.
 *
 * @author nickpadilla
 *
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(KeycloakOAuth2SsoConfiguration.class)
public @interface EnableKeycloakOAuth2Sso {

}
