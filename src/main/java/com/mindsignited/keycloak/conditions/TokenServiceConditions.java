package com.mindsignited.keycloak.conditions;

import org.springframework.boot.autoconfigure.condition.ConditionOutcome;
import org.springframework.boot.autoconfigure.condition.SpringBootCondition;
import org.springframework.boot.bind.RelaxedPropertyResolver;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.env.Environment;
import org.springframework.core.type.AnnotatedTypeMetadata;
import org.springframework.util.StringUtils;

/**
 * Created by nicholaspadilla on 1/28/15.
 */
public class TokenServiceConditions {

    public static class TokenInfo extends SpringBootCondition {

        @Override
        public ConditionOutcome getMatchOutcome(ConditionContext context,
                                                AnnotatedTypeMetadata metadata) {
            Environment environment = context.getEnvironment();
            boolean preferTokenInfo = environment
                    .resolvePlaceholders(
                            "${spring.oauth2.resource.preferTokenInfo:${OAUTH2_RESOURCE_PREFERTOKENINFO:true}}")
                    .equals("true");
            boolean hasTokenInfo = !environment.resolvePlaceholders(
                    "${spring.oauth2.resource.tokenInfoUri:}").equals("");
            boolean hasUserInfo = !environment.resolvePlaceholders(
                    "${spring.oauth2.resource.userInfoUri:}").equals("");
            if (!hasUserInfo) {
                return ConditionOutcome.match("No user info provided");
            }
            if (hasTokenInfo) {
                if (preferTokenInfo) {
                    return ConditionOutcome
                            .match("Token info endpoint is preferred and user info provided");
                }
            }
            return ConditionOutcome.noMatch("Token info endpoint is not provided");
        }

    }

    public static class JwtToken extends SpringBootCondition {

        @Override
        public ConditionOutcome getMatchOutcome(ConditionContext context,
                                                AnnotatedTypeMetadata metadata) {
            if (StringUtils.hasText(context.getEnvironment().getProperty(
                    "spring.oauth2.resource.jwt.keyValue"))
                    || StringUtils.hasText(context.getEnvironment().getProperty(
                    "spring.oauth2.resource.jwt.keyUri"))) {
                return ConditionOutcome.match("Public key is provided");
            }
            return ConditionOutcome.noMatch("Public key is not provided");
        }

    }

    public static class NotTokenInfo extends SpringBootCondition {

        private TokenInfo opposite = new TokenInfo();

        @Override
        public ConditionOutcome getMatchOutcome(ConditionContext context,
                                                AnnotatedTypeMetadata metadata) {
            ConditionOutcome outcome = opposite.getMatchOutcome(context, metadata);
            if (outcome.isMatch()) {
                return ConditionOutcome.noMatch(outcome.getMessage());
            }
            return ConditionOutcome.match(outcome.getMessage());
        }

    }

    public static class NotJwtToken extends SpringBootCondition {

        private JwtToken opposite = new JwtToken();

        @Override
        public ConditionOutcome getMatchOutcome(ConditionContext context,
                                                AnnotatedTypeMetadata metadata) {
            ConditionOutcome outcome = opposite.getMatchOutcome(context, metadata);
            if (outcome.isMatch()) {
                return ConditionOutcome.noMatch(outcome.getMessage());
            }
            return ConditionOutcome.match(outcome.getMessage());
        }

    }

    public static class ResourceServerCondition extends SpringBootCondition {

        @Override
        public ConditionOutcome getMatchOutcome(ConditionContext context,
                                                AnnotatedTypeMetadata metadata) {
            Environment environment = context.getEnvironment();
            RelaxedPropertyResolver resolver = new RelaxedPropertyResolver(environment);
            String client = environment.resolvePlaceholders("${spring.oauth2.client.clientId:}");
            if (StringUtils.hasText(client)) {
                return ConditionOutcome.match("found client id");
            }
            if (!resolver.getSubProperties("spring.oauth2.resource.jwt").isEmpty()) {
                return ConditionOutcome.match("found JWT resource configuration");
            }
            if (!resolver.getSubProperties("spring.oauth2.resource.userInfoUri").isEmpty()) {
                return ConditionOutcome.match("found UserInfo URI resource configuration");
            }
            return ConditionOutcome.noMatch("found neither client id nor JWT resource");
        }

    }

}
