# Common Configuration and Utilities

---
You will need to run this command to build and send the build to the maven repo.

   `mvn clean deploy -DperformRelease -Dwagon.git.debug=true`

To just test the build you can run it with this, this will not deploy anything to the GIT Repo
 
   `-Dwagon.git.safe.checkout=true`

Please see this - http://synergian.github.io/wagon-git/troubleshooting.html - for more information.
---

End. :)
